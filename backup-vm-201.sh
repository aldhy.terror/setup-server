echo ""
echo "************************** SHOW PARTITION SYSTEM  *************************"
echo "***************************************************************************"

lsblk -l

read -p "Please input External Drive Location: " DRIVE_LOCATION

mount -t ntfs-3g /dev/$DRIVE_LOCATION /var/backup-vm/

echo ""
echo "******************************* COPY DATA VM  *****************************"
echo "***************************************************************************"

rsync -ah --progress /var/lib/vz/dump /var/backup-vm/backup-vm-201-`date +%d-%m-%Y`

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "*              Don't forget to umount your External Drive                 *"
echo "***************************************************************************"