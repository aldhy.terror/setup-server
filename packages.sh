echo ""
echo "******************** INSTALL UTILITIES AND REPOSITORY  ********************"
echo "***************************************************************************"

yum -y install yum-utils
yum -y install epel-release
yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "******************** UPDATE & INSTALL ANOTHER PACKAGE  ********************"
echo "***************************************************************************"

yum update -y
yum -y install git curl wget unzip nano

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "******************************* INSTALL PHP *******************************"
echo "***************************************************************************"

read -p "Please input PHP Version (71 | 72 | 73 | 74): " PHP_VERSION

yum-config-manager --enable remi-php$PHP_VERSION

yum -y install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo php-common php-cli php-mbstring php-embedded php-php-bcmath php-opcache php-xml php-process php-pdo php-json php-pgsql


echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "***************************** INSTALL APACHE2 *****************************"
echo "***************************************************************************"

yum -y install httpd

echo ""
echo "************************ START AND ENABLE APACHE2 *************************"
echo "***************************************************************************"

systemctl start httpd.service
systemctl enable httpd.service
service httpd configtest

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "******************************* INSTALL SSL *******************************"
echo "***************************************************************************"

export SSL_PATH=/etc/httpd/conf.d/ssl.conf

yum -y install mod_ssl

echo ""
echo "************************ COPY FILE CERTIFICATE SSL ************************"
echo "***************************************************************************"

rsync -ah --progress -azP -e "ssh -p 22" root@192.168.5.202:/etc/httpd/ssl/ /etc/httpd/ssl/

echo ""
echo "************************** COPY FILE CONFIG SSL ***************************"
echo "***************************************************************************"

rm /etc/httpd/conf.d/ssl.conf
rsync -ah --progress -vz -e "ssh -p 22" root@192.168.5.202:/etc/httpd/conf.d/ssl.conf /etc/httpd/conf.d/

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "*************************** INSTALL POSTGRESQL ****************************"
echo "***************************************************************************"

read -p "Please input Postgresql Version (11 | 12 | 13 | 14): " PGSQL_VERSION

yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
yum -y install postgresql$PGSQL_VERSION-server
yum -y install postgresql$PGSQL_VERSION-contrib

echo ""
echo "************************** INIT DB POSTGRESQL $PGSQL_VERSION **************************"
echo "***************************************************************************"

sudo /usr/pgsql-$PGSQL_VERSION/bin/postgresql-$PGSQL_VERSION-setup initdb

echo ""
echo "********************* START AND ENABLE POSTGRESQL $PGSQL_VERSION **********************"
echo "***************************************************************************"

systemctl enable postgresql-$PGSQL_VERSION
systemctl start postgresql-$PGSQL_VERSION

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "****************** INSTALL FIGLET, COWSAY, LOLCAT, ZSH ********************"
echo "***************************************************************************"

yum -y install figlet cowsay ruby zsh

chsh -s /bin/zsh root
echo $SHELL
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
/bin/cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
source ~/.zshrc

git clone https://github.com/busyloop/lolcat.git
cd lolcat/bin
gem install lolcat

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "*************************** CONFIGURE HOSTNAME ****************************"
echo "***************************************************************************"

export HOSTS_FILE_PATH=/etc/hosts

read -p "Please input IP Address (127.0.0.1): " IP_HOSTNAME
read -p "Please input Hostname (new-virtual-machine.server): " NEW_HOSTNAME

hostnamectl set-hostname "$NEW_HOSTNAME"

echo "$IP_HOSTNAME   $NEW_HOSTNAME" >> $HOSTS_FILE_PATH

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "************************* CONFIGURE HTTPD.CONF ****************************"
echo "***************************************************************************"

export HTTPD_FILE_PATH=/etc/httpd/conf/httpd.conf
read -p "Please input your domain (example.com): " DOMAIN_NAME

sudo mkdir /var/www/html/$DOMAIN_NAME
sudo mkdir /var/log/httpd/$DOMAIN_NAME

echo "
<VirtualHost *:80>
    ServerName $DOMAIN_NAME
    ServerAlias www.$DOMAIN_NAME
    ServerAdmin webmaster@riaindahmandiri.com
    DocumentRoot /var/www/html/$DOMAIN_NAME
        ErrorLog /var/log/httpd/$DOMAIN_NAME/error_log 
        CustomLog /var/log/httpd/$DOMAIN_NAME/access_log combined
    <Directory /var/www/html/$DOMAIN_NAME>
        Options -Indexes +FollowSymLinks
        AllowOverride All
    </Directory>
</VirtualHost>

<VirtualHost *:443>
    ServerName $DOMAIN_NAME
    ServerAlias www.$DOMAIN_NAME
    ServerAdmin webmaster@riaindahmandiri.com
    SSLEngine on
    SSLCertificateFile /etc/httpd/ssl/STAR_riaindahmandiri_com.crt
    SSLCertificateKeyFile /etc/httpd/ssl/commercial.key
    SSLCertificateChainFile /etc/httpd/ssl/ca_bundle_sectigo.crt
    SSLCACertificateFile /etc/httpd/ssl/ca_bundle_sectigo.crt
    DocumentRoot /var/www/html/$DOMAIN_NAME
        ErrorLog /var/log/httpd/$DOMAIN_NAME/ssl_error_log
        CustomLog /var/log/httpd/$DOMAIN_NAME/ssl_access_log combined
    <Directory /var/www/html/$DOMAIN_NAME>
        Options -Indexes +FollowSymLinks
        AllowOverride All
    </Directory>
</VirtualHost>" >> $HTTPD_FILE_PATH

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "*************************** CONFIGURE PHP.INI *****************************"
echo "***************************************************************************"

export PHP_INI_PATH=/etc/php.ini

read -p "Please input Max Execution Time (second): " MAX_EXECUTION_TIME
read -p "Please input Max File Uploads Time (second): " MAX_FILE_UPLOADS
read -p "Please input Max Input Time (second): " MAX_INPUT_TIME
read -p "Please input Memory Limit (xxxM): " MAX_MEMORY_LIMIT
read -p "Please input Post Max Size (xxxM): " POST_MAX_SIZE

sed -i "s/max_execution_time = 30/max_execution_time = $MAX_EXECUTION_TIME/" $PHP_INI_PATH
sed -i "s/max_file_uploads = 20/max_file_uploads = $MAX_FILE_UPLOADS/" $PHP_INI_PATH
sed -i "s/max_input_time = 60/max_input_time = $MAX_INPUT_TIME/" $PHP_INI_PATH
sed -i "s/memory_limit = 128M/memory_limit = $MAX_MEMORY_LIMIT/" $PHP_INI_PATH
sed -i "s/post_max_size = 8M/post_max_size = $POST_MAX_SIZE/" $PHP_INI_PATH

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "***************************************************************************"

echo ""
echo "************************* FIREWALL CONFIGURATION **************************"
echo "***************************************************************************"

setsebool -P httpd_can_network_connect 1
firewall-cmd --permanent --zone=public --add-service=http 
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --permanent --zone=public --add-port=5432/tcp 

echo ""
echo "***************************** RELOAD FIREWALL *****************************"
echo "***************************************************************************"

firewall-cmd --reload

echo ""
echo "***************************************************************************"
echo "*                                 DONE                                    *"
echo "*                  DON'T FORGET TO CONFIGURATION FILE                     *"
echo "*                                                                         *"
echo "* 1. nano /var/lib/pgsql/$PGSQL_VERSION/data/pg_hba.conf                              *"
echo "* 2. nano /var/lib/pgsql/$PGSQL_VERSION/data/postgresql.conf                          *"
echo "*                                                                         *"
echo "***************************************************************************"